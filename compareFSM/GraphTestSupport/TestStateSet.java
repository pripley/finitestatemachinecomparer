package GraphTestSupport;

import junit.framework.*;
import junit.extensions.*;
import java.util.*;
import GraphSupport.*;

public class TestStateSet extends TestCase {

  private State m_S1;
  private State m_S2;
  private State m_S3;
  private State m_S4;
  private State m_S5;
  private StateSet m_SS1;
  private StateSet m_SS2;
  private StateSet m_SS3;
  private StateSet m_SS4;

  public TestStateSet(String testName) {
    System.out.println(testName);
  }

  protected void setUp() {

    try {
      m_S1 = new State("1");
      m_S2 = new State("2");
      m_S3 = new State("1");
      m_S4 = new State("4");
      m_S5 = new State("5");
      m_SS1 = new StateSet();
      m_SS2 = new StateSet();
      m_SS3 = new StateSet();
      m_SS4 = new StateSet();
      m_SS1.add(m_S1);
      m_SS1.add(m_S2);
      m_SS1.add(m_S4);
      m_SS2.add(m_S3);
      m_SS2.add(m_S4);
      m_SS3.add(m_S1);
      m_SS3.add(m_S5);
      m_SS3.add(m_S5);
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  public void testContainsState() {
    Assert.assertEquals(true, m_SS1.contains(m_S2));
    Assert.assertEquals(true, m_SS1.contains(m_S3));
    Assert.assertEquals(false, m_SS1.contains(m_S5));
    Assert.assertEquals(3, m_SS1.size());
    Assert.assertEquals(2, m_SS3.size());
  }
  public void testContainsSetOfStates() {
    Assert.assertEquals(false, m_SS1.contains(m_SS3));
    Assert.assertEquals(true, m_SS1.contains(m_SS2));
    Assert.assertEquals(true, m_SS1.contains(m_SS4));
  }
}