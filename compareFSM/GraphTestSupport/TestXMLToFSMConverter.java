package GraphTestSupport;

import junit.framework.*;
import junit.extensions.*;
import java.util.*;
import GraphSupport.*;

public class TestXMLToFSMConverter extends TestCase {
  private FiniteAutomaton m_FSM;
  private State m_S1;
  private State m_S2;
  private State m_S3;
  private State m_S4;
  private Transition m_T1;
  private Transition m_T2;
  private Transition m_T3;

  public TestXMLToFSMConverter(String testName) {
    System.out.println(testName);
  }

  protected void setUp() {

    try {

      m_FSM = XMLToFSMConverter.parse("GraphTestSupport/sampleFSM.xml");
          
      m_S1 = new State("1");
      m_S2 = new State("2");
      m_S3 = new State("7");
      m_S4 = new State("3");
      Action a = new Action("a");
      Action b = new Action("b");
      m_T1 = new Transition(m_S1, a, m_S2);
      m_T2 = new Transition(m_S1, a, m_S4);
      m_T3 = new Transition(m_S1, b, m_S2);
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  public void testParse() {
    testIsInAutomaton();
    testIsFinalState();
    testIsTransition();
    testIsStartingState();
  }
 
  private void testIsInAutomaton() {
    Assert.assertEquals(true, m_FSM.isInAutomaton(m_S2));
    Assert.assertEquals(true, m_FSM.isInAutomaton(m_S1));
    Assert.assertEquals(false, m_FSM.isInAutomaton(m_S3));
  }

  private void testIsFinalState() {
    Assert.assertEquals(true, m_FSM.isFinalState(m_S4));
    Assert.assertEquals(false, m_FSM.isFinalState(m_S1));
  }

  private void testIsTransition() {
    Assert.assertEquals(true, m_FSM.isTransition(m_T1));    
    Assert.assertEquals(false, m_FSM.isTransition(m_T2));
    Assert.assertEquals(false, m_FSM.isTransition(m_T3));
  }
  
  private void testIsStartingState() {
    Assert.assertEquals(true, m_FSM.isStartingState(m_S1));
    Assert.assertEquals(false, m_FSM.isStartingState(m_S3));
  }
}