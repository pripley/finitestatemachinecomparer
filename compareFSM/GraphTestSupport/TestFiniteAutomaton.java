package GraphTestSupport;

import junit.framework.*;
import junit.extensions.*;
import java.util.*;
import GraphSupport.*;

public class TestFiniteAutomaton extends TestCase {

  private Transition m_T1;
  private Transition m_T2;
  private Transition m_T3;
  private Transition m_T4;
  private Transition m_T5;
  private Transition m_T6;
  private Transition m_T7;
  private TransitionSet m_TS1;
  private TransitionSet m_TS2;
  private Action m_A1;
  private Action m_A2;
  private Action m_A3;
  private StateSet m_SS1;
  private StateSet m_SS2;
  private State m_S1;
  private State m_S2;
  private State m_S3;
  private State m_S4;
  private State m_S5;
  private State m_S6;
  private State m_S7;
  private State m_S8;
  private State m_S9;
  private PairOfStatesSet m_PSS1;
  private PairOfStatesSet m_PSS2;
  private PairOfStatesSet m_PSS3;
  private PairOfStatesSet m_PSS4;
  private PairOfStates m_PS1;  
  private PairOfStates m_PS2;
  private PairOfStates m_PS3;
  private PairOfStates m_PS4;

  private FiniteAutomaton m_FA1;
  private FiniteAutomaton m_FA2;

  public TestFiniteAutomaton(String testName) {
    System.out.println(testName);
  }

  protected void setUp() {

    try {
      m_A1 = new Action("a");
      m_A2 = new Action("b");
      m_A3 = new Action("c");
      m_S1 = new State("q0");
      m_S2 = new State("q1");
      m_S3 = new State("q2");
      m_S4 = new State("q3");
      m_S5 = new State("p0");
      m_S6 = new State("p1");
      m_S7 = new State("p2");
      m_S8 = new State("p3");
      m_S9 = new State("p4");
      m_SS1 = new StateSet();
      m_SS2 = new StateSet();
      m_SS1.add(m_S1);
      m_SS1.add(m_S2);
      m_SS1.add(m_S3);
      m_SS1.add(m_S4);
      m_SS2.add(m_S5);
      m_SS2.add(m_S6);
      m_SS2.add(m_S7);
      m_SS2.add(m_S8);
      m_SS2.add(m_S9);

      m_T1 = new Transition(m_S1, m_A1, m_S2);
      m_T2 = new Transition(m_S2, m_A2, m_S3);
      m_T3 = new Transition(m_S2, m_A3, m_S4);
      m_T4 = new Transition(m_S5, m_A1, m_S6);
      m_T5 = new Transition(m_S5, m_A1, m_S7);
      m_T6 = new Transition(m_S6, m_A2, m_S8);
      m_T7 = new Transition(m_S7, m_A3, m_S9);
      m_TS1 = new TransitionSet();
      m_TS2 = new TransitionSet();
      m_TS1.add(m_T1);
      m_TS1.add(m_T2);
      m_TS1.add(m_T3);
      m_TS2.add(m_T4);      
      m_TS2.add(m_T5);
      m_TS2.add(m_T6);
      m_TS2.add(m_T7);

      m_FA1 = new FiniteAutomaton(m_SS1, m_S1, m_SS1, m_TS1);
      m_FA2 = new FiniteAutomaton(m_SS2, m_S5, m_SS2, m_TS2);
      m_PSS1 = m_FA1.findEquivalentTransition(m_T1, m_S5, m_FA2);      
      m_PSS2 = new PairOfStatesSet();
      m_PS1 = new PairOfStates(m_S2, m_S6);
      m_PS2 = new PairOfStates(m_S2, m_S7);
      m_PSS2.add(m_PS1);
      m_PSS2.add(m_PS2);
      m_PSS2.print();
      System.out.println("");
      m_PSS1.print();
      m_PSS3 = m_FA2.findEquivalentStates(m_S5, m_S1, m_FA1);      
      System.out.println("");
      m_PSS3.print();
      m_PSS4 = new PairOfStatesSet();
      m_PSS4.add(m_PS1);
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  public void testFindEquivalentTransition() {
    Assert.assertEquals(true, m_PSS1.equals(m_PSS2));
    m_PSS1 = m_FA2.findEquivalentTransition(m_T4, m_S1, m_FA1);
    Assert.assertEquals(true, m_PSS4.equals(m_PSS1));
System.out.println("EquivTransition complete");
    m_PSS1 = m_FA1.findEquivalentStates(m_S1, m_S5, m_FA2);      
    Assert.assertEquals(true, m_PSS1.equals(m_PSS2));
System.out.println("EquivState1Test complete");
    Assert.assertEquals(true, m_PSS3.equals(m_PSS2));
  }
}