package GraphSupport;

import java.io.*;
import java.util.*;

/**
*  Compares two Finite State Machines and determines whether strong simulations and bisimulations exist
*  Contains the "main" function
*/
public class AutomatonComparision {

     static String readMyFile(String f) { 
        String returnString = "";
        String record;

        try { 

	   FileReader fr     = new FileReader(f);
           BufferedReader br = new BufferedReader(fr);

           record = new String();
           while ((record = br.readLine()) != null) {
             
             record = record.trim();
             if (!(record.equals(""))) {
               record = record + ":";
             }

             returnString = returnString + record;
           } 

        } catch (IOException e) { 
           // catch possible io errors from readLine()
           System.out.println("Uh oh, got an IOException error!");
        }
       return returnString;
     } // end of readMyFile()

  static public void main(String[] args) throws Exception {
    try {
      
      String fileName1 = args[0];
      String fileName2 = args[1];
      
      FiniteAutomaton fsm1 = XMLToFSMConverter.parse(fileName1);    
      FiniteAutomaton fsm2 = XMLToFSMConverter.parse(fileName2);

      System.out.println("\nAre "+fileName2+" and "+fileName1+" strongly bisimilar?");
      if ((fsm1.isStronglyBisimilar(fsm2)) && (fsm2.isStronglyBisimilar(fsm1))) {
        System.out.println(" therefore "+fileName2+" and "+fileName1+" are bisimilar.");
      }
      System.out.println("\nDoes "+fileName2+" simulate "+fileName1+"?");
      if (fsm1.isStrongSimulation(fsm2)) {
        System.out.println(" therefore "+fileName2+" strongly simulates "+fileName1+".");
      }
      System.out.println("\nDoes "+fileName1+" simulate "+fileName2+"?");
      if (fsm2.isStrongSimulation(fsm1)) {
        System.out.println(" therefore "+fileName1+" strongly simulates "+fileName2+".");
      }

    } catch(Exception e) {
      e.printStackTrace();
    }
  }
}