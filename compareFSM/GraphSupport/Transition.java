package GraphSupport;

import java.io.*;
import java.util.*;

/**
* Represents a transition between two states of a finite state machine
*/
public class Transition {

  private State m_From;
  private Action m_Action;
  private State m_To;
  
  public Transition(State from, Action a, State to) {
    m_From = from;
    m_Action = a;
    m_To = to;
  }

  public State getFrom() {
    return m_From;
  }
  
  public Action getAction() {
    return m_Action;
  }

  public State getTo() {
    return m_To;
  }

  public boolean equals(Transition t) {
    if ((t.getFrom().equals(m_From)) && (t.getAction().equals(m_Action)) && (t.getTo().equals(m_To))) { 
      return true;
    }
    else { 
      return false;
    }
  }
  
  public boolean fromEquals(State s) {
    if (s.equals(m_From)) { return true; } else { return false; }
  }
  public boolean toEquals(State s) {
    if (s.equals(m_To)) { return true; } else { return false; }
  }
  public boolean actionEquals(Action a) {
    if (a.equals(m_Action)) { return true; } else { return false; }
  }
  public void print() {
    System.out.print(m_From.getName()+"-"+m_Action.getName()+"->"+m_To.getName());
  }
}