package GraphSupport;

import java.io.*;
import java.util.*;

/**
* Represents a finite state machine
*/
public class FiniteAutomaton {
  private StateSet m_States;
  private State m_StartingState;
  private StateSet m_FinalStates;
  private TransitionSet m_Transitions;

  public FiniteAutomaton(StateSet states, State startingState, StateSet finalStates, TransitionSet transitions) throws NotInStateSetException {
    m_States = states;
    m_StartingState = startingState;
    m_FinalStates = finalStates;
    m_Transitions = transitions;
    if (!(states.contains(startingState))) { throw new NotInStateSetException("Intial state not in state set"); }
    if (!(states.contains(finalStates))) { throw new NotInStateSetException("Final state(s) not in state set"); }
  }

  public boolean isInAutomaton(State s) {
    return m_States.contains(s);
  }
  public boolean isFinalState(State s) {
    return m_FinalStates.contains(s);
  }
  public boolean isTransition(Transition t) {
    return m_Transitions.contains(t);
  }
  public boolean isStartingState(State s) {
    return m_StartingState.equals(s);
  }

  /**
  *  Tests if this FSM is a strong simulation of fa
  */
  public boolean isStrongSimulation(FiniteAutomaton fa) {
    State P = m_StartingState;
    State Q = fa.getStartingState();
    PairOfStatesSet equivStates = new PairOfStatesSet();
    PairOfStates ps = new PairOfStates(P,Q);
    equivStates.add(ps);
    String reason = "";
    Iterator i = m_States.iterator();    
    StateSet ss1;
    boolean foundEquivState = false;
    while (i.hasNext()) {
      P = (State)i.next();
      ss1 = equivStates.findStatesWith(P);
      Iterator j = ss1.iterator();
      foundEquivState = false;
      while (j.hasNext()) {
        Q = (State)j.next();
        //System.out.println("P:"+P.getName()+" Q:"+Q.getName());
        try {   
          equivStates.add(findEquivalentStates(P,Q,fa));
          foundEquivState = true; 
        } catch(NotStrongBisimulation s) {
          reason = s.getReason(equivStates);
        }
      }
      if (!foundEquivState) {
        System.out.println(reason);
        return false;
      }

    }
    equivStates.print();
    return true;
  }


  /*
  * Used when testing if FSM simulates another FSM 
  * collects all pairs of states from the transitions out of a state P, with FA2 in state Q
  */
/*
  public PairOfStatesSet findSimilarStates(State P, State Q, FiniteAutomaton fa) throws NotSimulation {
    TransitionSet t = m_Transitions.getSubset(P);
    PairOfStatesSet sp = new PairOfStatesSet();
    Iterator i = t.iterator();
    Transition t1;
    while (i.hasNext()) {
      t1 = (Transition)i.next();
      try {
        sp.add(findEquivalentTransition(t1, Q, fa));
        foundState
      catch(NotStronglyBisimilar bs) {
        
      }       
    }
    return sp;
  }
*/  
  /**
  * tests if a FSM machine is strongly bisimilar to this FSM
  */
  public boolean isStronglyBisimilar(FiniteAutomaton fa) {
    State P = m_StartingState;
    State Q = fa.getStartingState();
    PairOfStatesSet equivStates = new PairOfStatesSet();
    PairOfStates ps = new PairOfStates(P,Q);
    equivStates.add(ps);

    Iterator i = m_States.iterator();    
    StateSet ss1;
    try {
      while (i.hasNext()) {
        P = (State)i.next();
        ss1 = equivStates.findStatesWith(P);
        Iterator j = ss1.iterator();
        while (j.hasNext()) {
          Q = (State)j.next();
          //System.out.println("P:"+P.getName()+" Q:"+Q.getName());   
          equivStates.add(findEquivalentStates(P,Q,fa)); 
        }
      }
    } catch(NotStrongBisimulation sb) {
      sb.printReason(equivStates);
      return false;
    }
    equivStates.print();
    return true;
  }


  /*
  * Used when testing if a FSM is bisimular to another FSM 
  * collects all pairs of states from the transitions out of a state P, with FA2 in state Q
  */
  public PairOfStatesSet findEquivalentStates(State P, State Q, FiniteAutomaton fa) throws NotStrongBisimulation {
    TransitionSet t = m_Transitions.getSubset(P);
    PairOfStatesSet sp = new PairOfStatesSet();
    Iterator i = t.iterator();
    Transition t1;
    while (i.hasNext()) {
      t1 = (Transition)i.next();
      //t1.print();
      sp.add(findEquivalentTransition(t1, Q, fa));       
    }
    return sp;
  }

  /*
  * given a transition in FA1 and a state in FA2
  * this function collects all the states that simulate this transition
  */
  public PairOfStatesSet findEquivalentTransition(Transition t, State Q, FiniteAutomaton fa) throws NotStrongBisimulation {
    Action A = t.getAction();
    State to = t.getTo();
    State from = t.getFrom();
    if (!(fa.hasTransition(Q, A))) {  throw new NotStrongBisimulation(to, Q); }

    StateSet s = fa.getTransitionStates(Q,A);      
    PairOfStatesSet sp = new PairOfStatesSet();
    State s1;
    Iterator j = s.iterator();
    while (j.hasNext()) {
      s1 = (State)j.next();        
      sp.add(new PairOfStates(to, s1));
    }
    return sp;
  }


  public State getStartingState() {
    return m_StartingState;
  }
  
  /**
  * gets a set of "To" states that go from State Q with Action A
  */
  public StateSet getTransitionStates(State Q, Action A) {
    return m_Transitions.getTransitionStates(Q,A); 
  }

  public boolean hasTransition(State Q, Action A) {
    return m_Transitions.hasTransition(Q,A);
  }
}