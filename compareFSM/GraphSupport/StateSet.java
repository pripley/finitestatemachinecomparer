package GraphSupport;

import java.io.*;
import java.util.*;

/**
* Represents a set of states of a finite state machine
*/
public class StateSet {

  private Collection m_States;
  
  public StateSet() {
    m_States = new ArrayList();
  }
  
  public void add(State s) {
    if (contains(s)) { return; }
    m_States.add(s);
  }

  public int size() {
    return m_States.size();
  }

  
  public Iterator iterator() { return m_States.iterator(); }

  /**
  *  Checks if the inputted State is in this StateSet
  */
  public boolean contains(State s) {
    Iterator i = m_States.iterator();
    State s1;
    while (i.hasNext()) {
      s1 = (State)i.next();
      if (s.equals(s1)) { return true; }
    }
    return false;
  }
  
  /**
  * checks if the inputted StateSet is a subset of this StateSet
  */  
  public boolean contains(StateSet ss) {
    Iterator i = ss.iterator();
    State s1;
    while (i.hasNext()) {
      s1 = (State)i.next();
      if (!this.contains(s1)) { return false; }
    }
    return true;    
  }
}