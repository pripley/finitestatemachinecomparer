package GraphSupport;

import java.io.*;
import java.util.*;

/**
* Represents a set of transitions of a finite state machine
*/
public class TransitionSet {

  private Collection m_Transitions;
  
  public TransitionSet() {
    m_Transitions = new ArrayList();
  }
  
  public void add(Transition t) {
    if (contains(t)) { return; }
    m_Transitions.add(t);
  }
 
  public Iterator iterator() { return m_Transitions.iterator(); }

  public boolean contains(Transition t) {
    Iterator i = m_Transitions.iterator();
    Transition t1;
    while (i.hasNext()) {
      t1 = (Transition)i.next();
      if (t.equals(t1)) { return true; }
    }
    return false;
  }
  
  public int size() {
    return m_Transitions.size();
  }
  
  /**
  * checks if the inputted TransitionSet is a subset of this TransitionSet
  */  
  public boolean contains(TransitionSet ts) {
    Iterator i = ts.iterator();
    Transition t1;
    while (i.hasNext()) {
      t1 = (Transition)i.next();
      if (!this.contains(t1)) { return false; }
    }
    return true;    
  }

  /*
  * returns the set of "To"s that have the "From" state Q and action A
  */
  public StateSet getTransitionStates(State Q, Action A) {
    Iterator i = m_Transitions.iterator();
    Transition t1;
    StateSet ss = new StateSet();
    while (i.hasNext()) {
      t1 = (Transition)i.next();
      if ((t1.fromEquals(Q)) && (t1.actionEquals(A))) {
        ss.add(t1.getTo());
      }
    }
    return ss; 
  }
  
  public boolean hasTransition(State Q, Action A) {
    Iterator i = m_Transitions.iterator();
    Transition t1;
    while (i.hasNext()) {
      t1 = (Transition)i.next();
      if ((t1.fromEquals(Q)) && (t1.actionEquals(A))) {
        return true;
      }
    }  
    return false;  
  }

  public TransitionSet getSubset(State s) {
  Iterator i = m_Transitions.iterator();
    Transition t1;
    TransitionSet ts = new TransitionSet();
    while (i.hasNext()) {
      t1 = (Transition)i.next();
      if (t1.fromEquals(s)) { ts.add(t1); }
    }
    return ts;     
  }
}