package GraphSupport;

import java.io.*;
import java.util.*;
import javax.xml.parsers.*;
import org.xml.sax.*;
import org.xml.sax.helpers.*;

/**
*  I've used an XML parser to encode the inputted XML files into objects that can be compared
*/


/**
 * Program to count the number of elements using only the localName
 * component of the element, in an XML document.  Namespace names are
 * ignored for simplicity.  This example also shows one way to turn on
 * validation and how to use a SAX ErrorHandler.
 *
 * Notes: DefaultHandler is a SAX helper class that implements the SAX
 * ContentHandler interface by providing no-op methods.  This class
 * overrides some of the methods by extending DefaultHandler.  This program
 * turns on namespace processing and uses SAX2 interfaces to process XML
 * documents which may or may not be using namespaces.
 *
 * Update 2002-04-18: Added code that shows how to use JAXP 1.2 features to
 * support W3C XML Schema validation.  See the JAXP 1.2 maintenance review
 * specification for more information on these features.
 *
 *
 */

public class XMLToFSMConverter extends DefaultHandler {
    /** Constants used for JAXP 1.2 */
    static final String JAXP_SCHEMA_LANGUAGE =
        "http://java.sun.com/xml/jaxp/properties/schemaLanguage";
    static final String W3C_XML_SCHEMA =
        "http://www.w3.org/2001/XMLSchema";
    static final String JAXP_SCHEMA_SOURCE =
        "http://java.sun.com/xml/jaxp/properties/schemaSource";

    String m_CurrentTag;    
    public static final int STATES = 0;
    public static final int FINAL_STATES = 1;
    public static final int STARTING_STATE = 2;
    public static final int TRANSITIONS = 3;
    public static final int FROM = 0;
    public static final int ACTION = 1;
    public static final int TO = 2;
    private int m_CurrentTagGroup = -1;
    private String m_Transition_FromState = "";
    private String m_Transition_Action = "";
    private String m_Transition_ToState = "";
    private StateSet m_StateSet;
    private StateSet m_FinalStateSet;
    private State m_StartingState;
    private TransitionSet m_TransitionSet;
    private int m_TransitionTag = 0;
    private static FiniteAutomaton m_FSM;

    // Parser calls this once at the beginning of a document
    public void startDocument() throws SAXException {
       m_CurrentTag = "";
       m_StateSet = new StateSet();
       m_FinalStateSet = new StateSet();
       m_TransitionSet = new TransitionSet();
    }

    // Parser calls this for each element in a document
    public void startElement(String namespaceURI, String localName,String qName, Attributes atts) throws SAXException
    {
        m_CurrentTag = localName;
        if (localName.equals("States")) { m_CurrentTagGroup = STATES; }       
        if (localName.equals("FinalStates")) { m_CurrentTagGroup = FINAL_STATES; }
        if (localName.equals("StartingState")) { m_CurrentTagGroup = STARTING_STATE; }
        if (localName.equals("Transitions")) { m_CurrentTagGroup = TRANSITIONS; }
        if (localName.equals("From")) { m_TransitionTag = FROM; }
        if (localName.equals("Action")) { m_TransitionTag = ACTION; }
        if (localName.equals("To")) { m_TransitionTag = TO; }
    }

    // When the parser encounters plain text (not XML elements), it calls
    // this method, which accumulates them in a string buffer.
    // Note that this method may be called multiple times, even with no
    // intervening elements.
    public void characters(char[] buffer, int start, int length) {

        String value = new String (buffer, start, length);
        if ((value.trim().equals("")) || (value.equals("\n"))) { return; } 


        if ((m_CurrentTagGroup == STATES) && (m_CurrentTag.equals("Name"))) {
          State s = new State(value);
          m_StateSet.add(s);
        }
        if ((m_CurrentTagGroup == FINAL_STATES) && (m_CurrentTag.equals("Name"))) {
          State s = new State(value);
          m_FinalStateSet.add(s);
        }
        if ((m_CurrentTagGroup == STARTING_STATE) && (m_CurrentTag.equals("Name"))) {
          State s = new State(value);
          m_StartingState = s;
        }
        if ((m_CurrentTagGroup == TRANSITIONS) && (m_TransitionTag==FROM) && (m_CurrentTag.equals("Name"))) {
          m_Transition_FromState = value;          
        }
        if ((m_CurrentTagGroup == TRANSITIONS) && (m_TransitionTag==ACTION) && (m_CurrentTag.equals("Value"))) {
          m_Transition_Action = value;          
        }
        if ((m_CurrentTagGroup == TRANSITIONS) && (m_TransitionTag==TO) && (m_CurrentTag.equals("Name"))) {
          State fromState = new State(m_Transition_FromState);
          Action a = new Action(m_Transition_Action);
          Transition t = new Transition(fromState, a, new State(value));
          m_TransitionSet.add(t);
        }


    }


    // Parser calls this once after parsing a document
    public void endDocument() throws SAXException {
      try {
        m_FSM = new FiniteAutomaton(m_StateSet, m_StartingState, m_FinalStateSet, m_TransitionSet); 
      } catch(Exception e) {
        System.out.println("Parsing completed sucessfully, but there was an error creating the Automaton");
      }
    }

    /**
     * Convert from a filename to a file URL.
     */
    static private String convertToFileURL(String filename) {
        // On JDK 1.2 and later, simplify this to:
        // "path = file.toURL().toString()".
        String path = new File(filename).getAbsolutePath();
        if (File.separatorChar != '/') {
            path = path.replace(File.separatorChar, '/');
        }
        if (!path.startsWith("/")) {
            path = "/" + path;
        }
        return "file:" + path;
    }

    private static void usage() {
        System.err.println("Usage: SAXLocalNameCount [-options] <file.xml>");
        System.err.println("       -dtd = DTD validation");
        System.err.println("       -xsd | -xsdss <file.xsd> = W3C XML Schema validation using xsi: hints");
        System.err.println("           in instance document or schema source <file.xsd>");
        System.err.println("       -xsdss <file> = W3C XML Schema validation using schema source <file>");
        System.err.println("       -usage or -help = this message");
        System.exit(1);
    }

    static public FiniteAutomaton parse(String filename) throws Exception {
        
        boolean dtdValidate = false;
        boolean xsdValidate = false;
        String schemaSource = null;

        // There are several ways to parse a document using SAX and JAXP.
        // We show one approach here.  The first step is to bootstrap a
        // parser.  There are two ways: one is to use only the SAX API, the
        // other is to use the JAXP utility classes in the
        // javax.xml.parsers package.  We use the second approach here
        // because at the time of this writing it probably is the most
        // portable solution for a JAXP compatible parser.  After
        // bootstrapping a parser/XMLReader, there are several ways to
        // begin a parse.  In this example, we use the SAX API.

        // Create a JAXP SAXParserFactory and configure it
        SAXParserFactory spf = SAXParserFactory.newInstance();

        // Set namespaceAware to true to get a parser that corresponds to
        // the default SAX2 namespace feature setting.  This is necessary
        // because the default value from JAXP 1.0 was defined to be false.
        spf.setNamespaceAware(true);

        // Validation part 1: set whether validation is on
        spf.setValidating(dtdValidate || xsdValidate);

        // Create a JAXP SAXParser
        SAXParser saxParser = spf.newSAXParser();

        // Validation part 2a: set the schema language if necessary
        if (xsdValidate) {
            try {
                saxParser.setProperty(JAXP_SCHEMA_LANGUAGE, W3C_XML_SCHEMA);
            } catch (SAXNotRecognizedException x) {
                // This can happen if the parser does not support JAXP 1.2
                System.err.println(
                    "Error: JAXP SAXParser property not recognized: "
                    + JAXP_SCHEMA_LANGUAGE);
                System.err.println(
                    "Check to see if parser conforms to JAXP 1.2 spec.");
                System.exit(1);
            }
        }

        // Validation part 2b: Set the schema source, if any.  See the JAXP
        // 1.2 maintenance update specification for more complex usages of
        // this feature.
        if (schemaSource != null) {
            saxParser.setProperty(JAXP_SCHEMA_SOURCE, new File(schemaSource));
        }

        // Get the encapsulated SAX XMLReader
        XMLReader xmlReader = saxParser.getXMLReader();

        // Set the ContentHandler of the XMLReader
        xmlReader.setContentHandler(new XMLToFSMConverter());

        // Set an ErrorHandler before parsing
        xmlReader.setErrorHandler(new MyErrorHandler(System.err));

        // Tell the XMLReader to parse the XML document
        xmlReader.parse(convertToFileURL(filename));
        return m_FSM;      
    }

    // Error handler to report errors and warnings
    private static class MyErrorHandler implements ErrorHandler {
        /** Error handler output goes here */
        private PrintStream out;

        MyErrorHandler(PrintStream out) {
            this.out = out;
        }

        /**
         * Returns a string describing parse exception details
         */
        private String getParseExceptionInfo(SAXParseException spe) {
            String systemId = spe.getSystemId();
            if (systemId == null) {
                systemId = "null";
            }
            String info = "URI=" + systemId +
                " Line=" + spe.getLineNumber() +
                ": " + spe.getMessage();
            return info;
        }

        // The following methods are standard SAX ErrorHandler methods.
        // See SAX documentation for more info.

        public void warning(SAXParseException spe) throws SAXException {
            out.println("Warning: " + getParseExceptionInfo(spe));
        }
        
        public void error(SAXParseException spe) throws SAXException {
            String message = "Error: " + getParseExceptionInfo(spe);
            throw new SAXException(message);
        }

        public void fatalError(SAXParseException spe) throws SAXException {
            String message = "Fatal Error: " + getParseExceptionInfo(spe);
            throw new SAXException(message);
        }
    }
}