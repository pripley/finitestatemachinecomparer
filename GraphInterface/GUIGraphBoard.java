package GraphInterface;

import javax.swing.*;
import java.awt.event.*;
import java.awt.*;
import javax.swing.event.*;
import java.util.*;
import GraphSupport.*;

public class GUIGraphBoard extends JPanel {

  private final ArrayList m_StateList;
  private final ArrayList m_TransitionList;
  private GUIGraphBuilder m_Builder;  
  public final static GUIState EMPTY_STATE = new GUIState("EMPTY_STATE", 0, 0, false, false);
  private GUIState m_TransitionFromState = EMPTY_STATE;

  public GUIGraphBoard(GUIGraphBuilder b) {
    m_Builder = b;
    m_StateList = new ArrayList();    
    m_TransitionList = new ArrayList();    

    addMouseListener(new MouseAdapter() {
        

      public void mousePressed(MouseEvent e) {
        m_TransitionFromState = null;
        if (m_Builder.getButtonSelected() == GUIGraphBuilder.STATE_DRAWING) {
          stateButtonPressed(e);
        }
        if (m_Builder.getButtonSelected() == GUIGraphBuilder.SELECT_OBJECT) {
          selectButtonPressed(e);
        }
        if (m_Builder.getButtonSelected() == GUIGraphBuilder.TRANSITION_DRAWING) {
          transitionButtonPressed(e);
        }
      }

      /**
      * if DrawState then draw State (circle) add a circle to list of circles
      */
      private void stateButtonPressed(MouseEvent e) {
        GUIState g = new GUIState(""+(m_StateList.size()+1), e.getX(), e.getY(), false, false);
        m_StateList.add(g);
      }

      /**
      *  if SelectObject then check if there's something at that point
      */
      private void selectButtonPressed(MouseEvent e) {
        GUIState gs = selectGUIState(e);
        m_Builder.setProperties(gs);
      }

      /**
      * returns the GUI state at the selected location
      * selects and returns a GUI state
      * deselects all other states
      */
      private GUIState selectGUIState(MouseEvent e) {
        int x = e.getX();
        int y = e.getY();
        boolean foundSelected = false; //used to stop multiple states from being selected simultaneously
        Iterator i = m_StateList.iterator();
        GUIState gs;
        GUIState selectedGUIState = EMPTY_STATE;
        while (i.hasNext()) {
          gs = (GUIState)i.next();
          if ((!foundSelected) && (x >= gs.getX()) && (x <= gs.getX()+50) && (y >= gs.getY()) && (y <= gs.getY()+50)) {
            gs.select();
            foundSelected = true;
            selectedGUIState = gs;
          }
          else {
            gs.deselect();
          }
        }
        return selectedGUIState;        
      }
       
      /*
      * if DrawTransition then check the state from where it's being called from
      * need to check if it's the first state or second state selected then add a line to list of lines 
      */      
      private void transitionButtonPressed(MouseEvent e) {
        GUIState gs = selectGUIState(e);
        m_Builder.setProperties(gs);
        if (!gs.equals(EMPTY_STATE)) {
          m_TransitionFromState = gs;
        }
      }
      
      public void mouseReleased(MouseEvent e) {
        if (m_Builder.getButtonSelected() == GUIGraphBuilder.TRANSITION_DRAWING) {         
          GUIState gs = selectGUIState(e);
          if (gs.equals(EMPTY_STATE)) { return; }
          GUITransition gt = new GUITransition(m_TransitionFromState, gs);
          m_TransitionList.add(gt);
        }
        updateUI();
         
      }
    });    
  }

  public Dimension getPreferredSize() {
    return new Dimension(500, 500);
  }

  public void init() { 
   
         
  }

 
  public void paintComponent(Graphics g) {
    super.paintComponent(g);  //paint background 
    Iterator i = m_TransitionList.iterator();
    GUITransition gt;
    while (i.hasNext()) {
      gt = (GUITransition)i.next();
      
      g.drawLine(gt.FromGetX() + 12, gt.FromGetY() + 12, gt.ToGetX() + 12, gt.ToGetY() + 12);       
    }
    i = m_StateList.iterator();
    GUIState gs;
    int x, y;
    while (i.hasNext()) {
      gs = (GUIState)i.next();
      x = gs.getX();
      y = gs.getY();
      g.drawOval(x, y, 50, 50);
      g.setColor(Color.yellow);
      g.fillOval(x+2, y+2, 48, 48);
      g.setColor(Color.black);
      g.drawString(gs.getName(), x + 25, y + 25);
      if (gs.isSelected()) { 
        g.setColor(Color.blue);
        g.drawOval(x-2 , y-2, 54, 54);
        g.setColor(Color.black);
      } 
      if (gs.isFinalState()) {
        g.drawOval(gs.getX()+2, gs.getY()+2, 46, 46);
      }
    }

  }

}