package GraphInterface;

import javax.swing.*;
import java.awt.event.*;
import java.awt.*;
import java.util.*;
import javax.swing.event.*;

public class GUIGraphBuilder extends JFrame {
  protected int m_ButtonSelected = 2;    
  public final static int STATE_DRAWING = 0;
  public final static int TRANSITION_DRAWING = 1;
  public final static int SELECT_OBJECT = 2;    
  private GUIGraphBoard m_Board;
  private JTextField m_NameText = new JTextField();
  private JTextField m_XText = new JTextField();
  private JTextField m_YText = new JTextField();
  private JCheckBox m_FinalStateCheckBox = new JCheckBox();
  private JCheckBox m_StartingStateCheckBox = new JCheckBox();


  private GUIGraphBuilder() {
    super("Graph Builder2");
  }

  private void buildUI() {

    addWindowListener(new WindowAdapter() {
      public void windowClosing(WindowEvent e) {
        System.exit(0);
      }  
    });
    
    //Main Panel (control panel + board panel)
    //JPanel mainPanel = new JPanel(new GridLayout(0,2));
    JPanel mainPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
    //JPanel mainPanel = new JPanel(new ViewportLayout());
    //Control Panel (button panel + properties panel)
    JPanel controlPanel = new JPanel(new GridLayout(2,0));
    
    //Button Panel
    JPanel buttonPanel = new JPanel(new GridLayout(3,1)); 

    JButton b1 = new JButton("State");
    b1.addMouseListener(new MouseAdapter() {
      public void mouseClicked(MouseEvent e) {
        m_ButtonSelected = STATE_DRAWING;
      }
    });
    JButton b2 = new JButton("Transition");
    b2.addMouseListener(new MouseAdapter() {
      public void mouseClicked(MouseEvent e) {
        m_ButtonSelected = TRANSITION_DRAWING;
      }
    });
    JButton b3 = new JButton("Select Object");
    b3.addMouseListener(new MouseAdapter() {
      public void mouseClicked(MouseEvent e) {
        m_ButtonSelected = SELECT_OBJECT;
      }
    });

    buttonPanel.add(b1);
    buttonPanel.add(b2);
    buttonPanel.add(b3);
    controlPanel.add(buttonPanel);

    //State Properties Panel
    JPanel propertiesPanel = new JPanel(new GridLayout(6,2));

    JLabel nameLabel = new JLabel("Name:");
    JLabel XLabel = new JLabel("X:");
    JLabel YLabel = new JLabel("Y:");
    JLabel finalStateLabel = new JLabel("Final State:");
    JLabel startingStateLabel = new JLabel("Starting State:");
    propertiesPanel.add(nameLabel);
    propertiesPanel.add(m_NameText);
    propertiesPanel.add(XLabel);
    propertiesPanel.add(m_XText);
    propertiesPanel.add(YLabel);
    propertiesPanel.add(m_YText);
    propertiesPanel.add(finalStateLabel);
    propertiesPanel.add(m_FinalStateCheckBox);
    propertiesPanel.add(startingStateLabel);
    propertiesPanel.add(m_StartingStateCheckBox);

    JButton updatePropertiesButton = new JButton("Update Properties");
    updatePropertiesButton.addMouseListener(new MouseAdapter() {
      public void mouseClicked(MouseEvent e) {
        //set properties
        
        //updateUI();
      }
    });
    propertiesPanel.add(updatePropertiesButton);
    controlPanel.add(propertiesPanel);
    controlPanel.setSize(50, 600);
    
    mainPanel.add(controlPanel);

    //graph drawing board
    GUIGraphBoard m_Board = new GUIGraphBoard(this);
    mainPanel.add(m_Board);

    getContentPane().add(mainPanel);    
    pack();
    setVisible(true);
  }
  
  public int getButtonSelected() {
    return m_ButtonSelected;
  }

  public void setProperties(GUIState gs) {
    m_NameText.setText(gs.getName());
    m_XText.setText(String.valueOf(gs.getX()));
    m_YText.setText(String.valueOf(gs.getY()));
    m_FinalStateCheckBox.setSelected(gs.isFinalState());
    m_StartingStateCheckBox.setSelected(gs.isStartingState());
  }

  public static void main(String args[]) {
    System.out.println("Starting GUI");
    GUIGraphBuilder g = new GUIGraphBuilder();
    g.buildUI();
  }
  
  
}