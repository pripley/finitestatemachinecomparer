package GraphInterface;

public class GUIState {

  private String m_Name;
  private int m_X;
  private int m_Y;
  private boolean m_IsFinalState;
  private boolean m_IsStartingState;
  private boolean m_Selected = false;
  
  public GUIState(String name, int X, int Y, boolean isFinalState, boolean isStartingState) {
    m_Name = name;
    m_X = X;
    m_Y = Y;
    m_IsFinalState = isFinalState;
    m_IsStartingState = isStartingState;    
  }

  public String getName() {
    return m_Name;
  }

  public int getX() {
    return m_X;
  }

  public int getY() {
    return m_Y;
  }
  public boolean equals(GUIState gs) {
    if (gs.getName().equals(m_Name)) { return true; } else { return false; }
  }
  public boolean isFinalState() {
    return m_IsFinalState;
  }

  public boolean isStartingState() {
    return m_IsStartingState;
  }
  
  public boolean isSelected() {
    return m_Selected;
  }
  
  public void select() {
    m_Selected = true;
  }
  
  public void deselect() {
    m_Selected = false;
  }
}