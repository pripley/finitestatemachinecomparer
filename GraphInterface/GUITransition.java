package GraphInterface;

public class GUITransition {

  private String m_Action;
  private int m_X;
  private int m_Y;
  private GUIState m_From;
  private GUIState m_To;
  
  public GUITransition(GUIState from, GUIState to) {
    m_From = from;
    m_To = to;
    m_Action = "a";
  }

  public void setAction(String s) {
    m_Action = s;
  }

  public int FromGetX() {
    return m_From.getX();
  }

  public int FromGetY() {
    return m_From.getY();
  }

  public int ToGetX() {
    return m_To.getX();
  }

  public int ToGetY() {
    return m_To.getY();
  }


  public boolean equals(GUITransition gt) {
    if ((gt.FromEquals(m_From)) && (gt.ToEquals(m_To)) && (gt.ActionEquals(m_Action))) { return true; } else { return false; }
  }
  
  public boolean FromEquals(GUIState gs) {
    return m_From.equals(gs);
  }

  public boolean ToEquals(GUIState gs) {
    return m_To.equals(gs);
  }

  public boolean ActionEquals(String a) {
    return m_Action.equals(a);
  }
/*
  public boolean isSelected() {
    return m_Selected;
  }
  
  public void select() {
    m_Selected = true;
  }
  
  public void deselect() {
    m_Selected = false;
  }
*/
}