package GraphSupport;

public class NotInStateSetException extends Exception {
  public NotInStateSetException(String s) {
    new Exception(s);
  }
  public NotInStateSetException() {
    new Exception();
  }
}