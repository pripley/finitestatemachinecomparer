package GraphSupport;

import java.io.*;
import java.util.*;

public class SVGFSMPrinter {

  private String m_OutputFile;
  private int m_ScreenSizeX;
  private int m_ScreenSizeY;
  private int m_FontSize;
  private int m_StartX;
  private int m_StartY;
  private static FileWriter m_Out;
  private int m_LineHeight;

  public SVGProofPrinter() {
    m_ScreenSizeX = 600;
    m_ScreenSizeY = 600;
    m_FontSize = 12;
    m_StartX = 25;
    m_StartY = 10;
    m_OutputFile = "Automaton.svg";
    m_LineHeight = 16;
  }
  
  public void generateSVG(ArrayList a) {
    try {
      String outputFile = System.getProperty("user.dir") + System.getProperty("file.separator")+m_OutputFile;
      File file = new File(outputFile);
      m_Out = new FileWriter(file);
      
      writeFile("<?xml version=\"1.0\" standalone=\"no\"?>");
      writeFile("<!DOCTYPE svg PUBLIC \"-//W3C//DTD SVG 20000303 Stylable//EN\"");
      writeFile("\"http://www.w3.org/TR/2000/03/WD-SVG-20000303/STD/svg-20000303-stylable.dtd\">");
      writeFile("<svg xml:space=\"preserve\" width=\""+m_ScreenSizeX+"\" height=\""+m_ScreenSizeY+"\"");
      writeFile("xmlns=\"http://www.w3.org/2000/svg-20000303-stylable\" zoomAndPan=\"magnify\">");
      writeFile("<g id=\"Automaton\">");
      buildSVG(a,m_StartX,m_StartY);
      writeFile("</g></svg>");
      m_Out.close();
    }
    catch(Exception e) {
      System.out.println("A problem occurred building the SVG document.");
    }
  }

  private void buildSVG(FiniteAutomaton a, int startX, int startY) {
    
    Iterator j = a.listIterator();
    int y = startY;
    while (j.hasNext()) {
      ProofNode n = (ProofNode)j.next();
      writeFile("<g id=\"lineNum"+ n.getLineNum() +"\" transform=\"matrix(1 0 0 1 0,"+y+")\"><text style=\"font-size:"+m_FontSize+";\">"+n.getLineNum()+"</text></g>");
      if (n.getRule().equals("hyp")) {
        HypNode hyp;
        try {
          hyp = (HypNode)n;
        }
        catch (Exception e) { System.out.println("Casting error occurred"); return; }
        
        writeFile("<g id=\"wff"+ (y - m_StartY) +"\" transform=\"matrix(1 0 0 1 "+startX+","+y+")\"><text style=\"font-size:"+m_FontSize+";\">"+hyp.getWFF().getFormula()+"</text></g>");
        writeFile("<g id=\"wffreason"+ (y - m_StartY) +"\" transform=\"matrix(1 0 0 1 "+(startX+100)+","+y+")\"><text style=\"font-size:"+m_FontSize+";\">"+hyp.getReason()+"</text></g>");
        writeFile("<g id=\""+(y - m_StartY)+"line\" style=\"fill:none;\"><line x1=\""+startX+"\" y1=\""+(y-(0.5*m_LineHeight))+"\" x2=\""+startX+"\" y2=\""+ ((hyp.getTotalLines() * m_LineHeight) + y) +"\" style=\"fill:none;stroke:#000000;stroke-width:1;opacity:none\"/></g>");
        writeFile("<g id=\""+(y - m_StartY)+"sline\" style=\"fill:none;\"><line x1=\""+startX+"\" y1=\""+y+"\" x2=\""+ (startX + 10) +"\" y2=\""+ y +"\" style=\"fill:none;stroke:#000000;stroke-width:1;opacity:none\"/></g>");
        buildSVG(hyp.getOnScope(), startX + 10, m_LineHeight + y);
        y = ((hyp.getTotalLines()+1) * m_LineHeight) + y;
      }
      else {
        writeFile("<g id=\"wff"+ (y - m_StartY) +"\" transform=\"matrix(1 0 0 1 "+startX+","+y+")\"><text style=\"font-size:"+m_FontSize+";\">"+n.getWFF().getFormula()+"</text></g>");
        writeFile("<g id=\"wffreason"+ (y - m_StartY) +"\" transform=\"matrix(1 0 0 1 "+(startX+100)+","+y+")\"><text style=\"font-size:"+m_FontSize+";\">"+n.getReason()+"</text></g>");
        y = y + m_LineHeight;
      }
          
    }
  }

  private void writeFile(String s) {
    try {
      m_Out.write(s+"\n");
    }
    catch(IOException e) {
      System.out.println("An error occurred writing to the file");
    }
  }
}
  