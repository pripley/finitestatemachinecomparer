package GraphSupport;

import java.io.*;
import java.util.*;

/**
* Respresents the action of a transition
*/
public class Action {

  private String m_Name;
  
  public Action(String name) {
    m_Name = name;
  }

  public String getName() {
    return m_Name;
  }
  
  public boolean equals(Action a) {
    if (a.getName().equals(m_Name)) { 
      return true;
    }
    else { 
      return false;
    }
  }
}