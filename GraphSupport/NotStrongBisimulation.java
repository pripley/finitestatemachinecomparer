package GraphSupport;

import java.io.*;
import java.util.*;

public class NotStrongBisimulation extends Error {
  private State m_State1;
  private State m_State2;

  NotStrongBisimulation(State s1, State s2) {
    m_State1 = s1;
    m_State2 = s2;
  }
  public void printReason(PairOfStatesSet pss) {
    System.out.println("  No, I could not find a mapping for "+m_State1.getName()+" when I am in state "+m_State2.getName()+".");
    System.out.println("However, I was able to find mappings for the following pairs of states.");
    pss.print();   
    System.out.println(""); 
  }
  public String getReason(PairOfStatesSet pss) {
    return "  No, I could not find a mapping for "+m_State1.getName()+"\nHowever, I was able to find mappings for the following pairs of states."+pss.getString()+"\n"; 
  }
}