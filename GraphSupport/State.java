package GraphSupport;

import java.io.*;
import java.util.*;

/**
* Represents a state in a finite state machine
*/
public class State {

  private String m_Name;
  
  public State(String name) {
    m_Name = name;
  }

  public String getName() {
    return m_Name;
  }
  public void print() {
    System.out.print(m_Name);
  }
  public boolean equals(State s) {
    if (m_Name.equals(s.getName())) { 
      return true;
    }
    else { 
      return false;
    }
  }
}