package GraphSupport;

import java.io.*;
import java.util.*;

/**
* Represents a pair of states
*/
public class PairOfStates {

  private State m_State1;
  private State m_State2;
  
  public PairOfStates(State s1, State s2) {
    m_State1 = s1;
    m_State2 = s2;
  }

  public boolean contains(State s) {
    if((m_State1.equals(s)) || (m_State2.equals(s))) { 
      return true;
    }
    else {
      return false;
    }
  }
  
  public boolean equals(PairOfStates ps) {
    if ((ps.contains(m_State1)) && (ps.contains(m_State2))) { 
      return true;
    }
    else { 
      return false;
    }
  }

  public State getPartner(State s) {
    if (m_State1.equals(s)) { return m_State2; }
    if (m_State2.equals(s)) { return m_State1; }
    return new State("emp");
  }

  public void print() {
    System.out.print("("+m_State1.getName()+","+m_State2.getName()+")");
  }
  public String getString() {
    return "("+m_State1.getName()+","+m_State2.getName()+")";
  }
}