package GraphSupport;

import java.io.*;
import java.util.*;

/**
* Represents a set of pairs of states
*/
public class PairOfStatesSet {

  private Collection m_PairOfStates;
  
  public PairOfStatesSet() {
    m_PairOfStates = new ArrayList();
  }
  
  public void add(PairOfStates s) {
    if (contains(s)) { return; }
    m_PairOfStates.add(s);
  }

  public int size() {
    return m_PairOfStates.size();
  }

  public void add(PairOfStatesSet ps) {
    Iterator i = ps.iterator();
    PairOfStates s1;
    while (i.hasNext()) {
      s1 = (PairOfStates)i.next();
      this.add(s1); 
    }
  }
  
  public boolean equals(PairOfStatesSet pss) {
    if ((pss.contains(this)) && (contains(pss))) { return true; } else { return false; }
  }
 
  public Iterator iterator() { return m_PairOfStates.iterator(); }

  /**
  *  Checks if the inputted PairOfStates is in this PairOfStatesSet
  */
  public boolean contains(PairOfStates s) {
    Iterator i = m_PairOfStates.iterator();
    PairOfStates s1;
    while (i.hasNext()) {
      s1 = (PairOfStates)i.next();
      if (s.equals(s1)) { return true; }
    }
    return false;
  }
  
  /**
  * checks if the inputted PairOfStatesSet is a subset of this PairOfStatesSet
  */  
  public boolean contains(PairOfStatesSet ss) {
    Iterator i = ss.iterator();
    PairOfStates s1;
    while (i.hasNext()) {
      s1 = (PairOfStates)i.next();
      if (!this.contains(s1)) { return false; }
    }
    return true;    
  }

  public StateSet findStatesWith(State s) {
    Iterator i = m_PairOfStates.iterator();
    PairOfStates s1;
    StateSet ss = new StateSet();
    while (i.hasNext()) {
      s1 = (PairOfStates)i.next();
      if (s1.contains(s)) { ss.add(s1.getPartner(s)); }
    }
    return ss;
  }

  public void print() {
    Iterator i = m_PairOfStates.iterator();
    PairOfStates s1;
    System.out.print("{");
    while (i.hasNext()) {
      s1 = (PairOfStates)i.next();
      s1.print();
    }
    System.out.print("}");  
  }

  public String getString() {
    Iterator i = m_PairOfStates.iterator();
    PairOfStates s1;
    String s = "{";
    while (i.hasNext()) {
      s1 = (PairOfStates)i.next();
      s = s + s1.getString();
    }
    s = s+"}";
    return s;   
  }
}