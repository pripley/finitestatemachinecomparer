package GraphTestSupport;

import junit.framework.*;
import junit.extensions.*;
import java.util.*;
import GraphSupport.*;

public class TestTransition extends TestCase {

  private State m_S1;
  private State m_S2;
  private State m_S3;
  private Transition m_T1;
  private Transition m_T2;
  private Transition m_T3;
  private Transition m_T4;
  private Transition m_T5;
  private Action m_A1;
  private Action m_A2;

  public TestTransition(String testName) {
    System.out.println(testName);
  }

  protected void setUp() {

    try {
      m_A1 = new Action("a");
      m_A2 = new Action("b");
      m_S1 = new State("1");
      m_S2 = new State("2");
      m_S3 = new State("1");
      m_T1 = new Transition(m_S1, m_A1, m_S2);
      m_T2 = new Transition(m_S1, m_A1, m_S3);
      m_T3 = new Transition(m_S3, m_A1, m_S2);
      m_T4 = new Transition(m_S1, m_A2, m_S2);
      m_T5 = new Transition(m_S2, m_A1, m_S1);
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  public void testEquals() {
    Assert.assertEquals(false, m_T1.equals(m_T2));
    Assert.assertEquals(true, m_T1.equals(m_T3));
    Assert.assertEquals(false, m_T1.equals(m_T5));
    Assert.assertEquals(false, m_T1.equals(m_T4));
  }
  
}