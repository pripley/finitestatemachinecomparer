package GraphTestSupport;

import junit.framework.*;
import junit.extensions.*;
import GraphSupport.*;

public class GraphTestSuite {
  public static Test suite() {
    TestSuite suite = new TestSuite();

    suite.addTest( new TestState("State equals") { 
      protected void runTest() { testEquals(); } 
    } );

    suite.addTest( new TestStateSet("StateSet contains (state)") { 
      protected void runTest() { testContainsState(); } 
    } );

    suite.addTest( new TestStateSet("StateSet contains (set)") { 
      protected void runTest() { testContainsSetOfStates(); } 
    } );    

    suite.addTest( new TestTransition("Transition equals") { 
      protected void runTest() { testEquals(); } 
    } );

    suite.addTest( new TestTransitionSet("TransitionSet contains (state)") { 
      protected void runTest() { testContainsTransition(); } 
    } );

    suite.addTest( new TestTransitionSet("TransitionSet contains (set)") { 
      protected void runTest() { testContainsSetOfTransitions(); } 
    } ); 
   
    suite.addTest( new TestXMLToFSMConverter("XMLToFSMConverter parse") { 
      protected void runTest() { testParse(); } 
    } ); 

    suite.addTest( new TestFiniteAutomaton("FiniteAutomaton findEquivalentTransition") { 
      protected void runTest() { testFindEquivalentTransition(); } 
    } ); 
    return suite;
  }

  public static void main (String[] args) {
    junit.textui.TestRunner.run (suite());
  }
}