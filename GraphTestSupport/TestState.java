package GraphTestSupport;

import junit.framework.*;
import junit.extensions.*;
import java.util.*;
import GraphSupport.*;

public class TestState extends TestCase {

  private State m_S1;
  private State m_S2;
  private State m_S3;

  public TestState(String testName) {
    System.out.println(testName);
  }

  protected void setUp() {

    try {
      m_S1 = new State("A");
      m_S2 = new State("2");
      m_S3 = new State("A");
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  public void testEquals() {
    Assert.assertEquals(false, m_S1.equals(m_S2));
    Assert.assertEquals(true, m_S1.equals(m_S1));
    Assert.assertEquals(true, m_S1.equals(m_S3));
  }
  
}