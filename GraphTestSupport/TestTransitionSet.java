package GraphTestSupport;

import junit.framework.*;
import junit.extensions.*;
import java.util.*;
import GraphSupport.*;

public class TestTransitionSet extends TestCase {

  private Transition m_T1;
  private Transition m_T2;
  private Transition m_T3;
  private Transition m_T4;
  private Transition m_T5;
  private TransitionSet m_TS1;
  private TransitionSet m_TS2;
  private TransitionSet m_TS3;
  private TransitionSet m_TS4;
  private Action m_A1;
  private Action m_A2;
  private State m_S1;
  private State m_S2;
  private State m_S3;
  private State m_S4;
  private State m_S5;
  
  public TestTransitionSet(String testName) {
    System.out.println(testName);
  }

  protected void setUp() {

    try {
      m_A1 = new Action("a");
      m_A2 = new Action("b");
      m_S1 = new State("1");
      m_S2 = new State("2");
      m_S3 = new State("1");
      m_S4 = new State("4");
      m_S5 = new State("5");
      m_T1 = new Transition(m_S1, m_A1, m_S2);
      m_T2 = new Transition(m_S1, m_A1, m_S1);
      m_T3 = new Transition(m_S1, m_A1, m_S3);
      m_T4 = new Transition(m_S1, m_A1, m_S4);
      m_T5 = new Transition(m_S4, m_A1, m_S5);
      m_TS1 = new TransitionSet();
      m_TS2 = new TransitionSet();
      m_TS3 = new TransitionSet();
      m_TS4 = new TransitionSet();
      m_TS1.add(m_T1);
      m_TS1.add(m_T2);
      m_TS1.add(m_T4);
      m_TS2.add(m_T3);
      m_TS2.add(m_T4);
      m_TS3.add(m_T1);
      m_TS3.add(m_T5);
      m_TS3.add(m_T5);
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  public void testContainsTransition() {
    Assert.assertEquals(true, m_TS1.contains(m_T2));
    Assert.assertEquals(true, m_TS1.contains(m_T3));
    Assert.assertEquals(false, m_TS1.contains(m_T5));
    Assert.assertEquals(3, m_TS1.size());
    Assert.assertEquals(2, m_TS3.size());
  }
  public void testContainsSetOfTransitions() {
    Assert.assertEquals(false, m_TS1.contains(m_TS3));
    Assert.assertEquals(true, m_TS1.contains(m_TS2));
    Assert.assertEquals(true, m_TS1.contains(m_TS4));
  }
}